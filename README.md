# Balena on the RasPI zero

This is a simple example for a multicontainer [BalenaOS](https://balena.io/os) setup for the Raspberry PI zero.

## Prepare the image and flash to SD card

To run this on a Pi Zero make sure to download and configure the base image with these instructions: https://www.balena.io/os/docs/raspberry-pi/getting-started/. The instructions for Raspberry Pi 1 are the same for the Zero. This includes to install the `balena-cli`. On macOSthis can be done with Homebrew: `brew install balena-cli`. Note that for configuring and later flashing the SD card you need admin priviliges to execute commands as `sudo`. For flashing the SD card the [Balena Etcher](https://www.balena.io/etcher/) is a very good choice.

### Important Steps when running balena local configure

Make sure to opt in the advanced configuration options with 'y' when prompted and give your PI a sensible hostname, e.g. `mypi.local`. This will make all future commands easier.

Follow the instructions until "Running your first container".

## Running this project

On your computer, open a Terminal window and `cd` into your project folder. Make sure you have the `balena-cli` installed.

NOTE: These instructions assume the hostname of the Pi to be `zerotest.local`.

In order to get the scripts onto the PI use `balena push`:

```
balena push zerotest.local
```

Wait for the commandline to run. You will see output of docker building the image and then should at some point print out `[mypythonservice] Hello World!` repeatedly.

## Building ontop of this

To change the behaviour of the script simply edit the python script in the directory `service1`. You can also rename the directory. In that case make sure to also adjust the directory name in the `docker-compose.yml` file so docker can find the service to build.

For further guides on how to configure the Dockerfile, access the GPIOs, etc, see the balena docs: 

## Using the GPIOs

The docker-compose.yml is already configured to run `service1` in priviliged mode. This is needed for balena to give the container access. See service 1 to see how it is using `pip` with a requirements.txt.

Here is the documentation for the Python Balena image: https://hub.docker.com/r/balenalib/raspberry-pi-alpine-python/

`pip` is already installed. We only added the `RPi.Gpio` package to the `requirements.txt`. This can also be seen in the playground exmaple from Balena: https://github.com/balena-io-playground/balena-rpi-gpio-sample-with-python.

The `service1` example uses the [gpiozero](https://gpiozero.readthedocs.io/en/stable/recipes.html) package which is one of the options to access the pins. It has nothing to do with the Pi being a Zero though!

## Notes

* Pushing to the Pi Zero the build phase can be very slow! Just be patient when a changed the `FROM` image in your Dockerfile

## Configuring hostname and wifi afterwards

To change the wifi settings or the hostname see here: https://www.balena.io/os/docs/raspberry-pi/getting-started/#Configure-balenaOS-without-the-CLI

## Other Pi boards and other base images for Dockerfile

To use another base image (e.g. debian), here is a list of images oyu can use for the `FROM` instruction in the Dockerfile: https://www.balena.io/docs/reference/base-images/base-images-ref/


## More resources

Local mode is very useful for prototyping: https://www.balena.io/docs/learn/develop/local-mode/
