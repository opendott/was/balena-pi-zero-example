import RPi.GPIO as GPIO
import time

from gpiozero import Button

button = Button("BOARD11") # Means pin 0 as for WiringPi, or BCMGPIO 17

def run():
    while True:
        # Change gpio pins in list 1 from low to high and list 2 from high to low
        if button.was_pressed:
            print("Button pressed")
        else:
            print("not button was pressed")
        time.sleep(1)


# Reset all gpio pin
def exit():
    GPIO.cleanup()

if __name__ == '__main__':
    run()
    exit()